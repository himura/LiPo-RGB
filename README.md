# 12V RGB-LED Strip Controller on a LiPo battery

![](photo.jpg)

![](PCB.png)

[Schematics](schema.pdf)


3.7V -> 12V Calculations
====

[LM2621](http://www.ti.com/lit/ds/symlink/lm2621.pdf)

### Transformation

Vin=3.7 V
Vout=11.57 V
Iout=400 mA

> Vout = (1.24*(R_F1 + R_F2))/R_F2

R_F1=150 kOhm
R_F2=18 kOhm
C_F1=39 pF

---

### Non-bootstrapped mode

Vdd=Vin (instead of transformed Vout)

---

### Frequency

FQ=1.75 Mhz
R_FQ=75 kOhm

---

### Shielded Inductor

L=6.8uH
ESR < 100mΩ
I_L_peak=1.7A

* https://www.chipdip.ru/product/srr0618-6r8m
* https://www.chipdip.ru/product/srr0604-6r8ml
* https://www.chipdip.ru/product/b82464g4682m

---

### Schottky Diode

I_fwd > Iout
V_rev > Vout

---

### Tantalum Caps

Cin=22uF, Vmax > Vin
Cout=68uF, Vmax > Vout
ESR < 3Ohm

