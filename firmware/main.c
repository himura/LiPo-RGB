#define F_CPU 1200000UL
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdlib.h>

#define BTN_PIN3  // BTN_PIN3 || BTN_PIN4

#ifdef BTN_PIN4
#define BTN_PIN PIN4
#define BTN_PCINT PCINT4
#else
#define BTN_PIN PIN3
#define BTN_PCINT PCINT3
#endif

#define BTN_PRESSED !(PINB & (1 << BTN_PIN))
#define BTN_INT_ENABLE  PCMSK |=  (1 << BTN_PCINT)
#define BTN_INT_DISABLE PCMSK &= ~(1 << BTN_PCINT)

#define G_ON  PORTB |=  (1 << PORTB0)  // OC0A
#define R_ON  PORTB |=  (1 << PORTB1)  // OC0B
#define B_ON  PORTB |=  (1 << PORTB2)
#define G_OFF PORTB &= ~(1 << PORTB0)
#define R_OFF PORTB &= ~(1 << PORTB1)
#define B_OFF PORTB &= ~(1 << PORTB2)

#define PWM_G_CONNECT    TCCR0A |=  (1 << COM0A1)
#define PWM_G_DISCONNECT TCCR0A &= ~(1 << COM0A1)
#define PWM_G_START PWM_G_CONNECT;    TCCR0B |=  (1 << CS00)
#define PWM_G_STOP  PWM_G_DISCONNECT; TCCR0B &= ~(1 << CS00)

#define WATCHDOG_TIMER_RESET MCUSR &= ~(1 << WDRF)

#define OFF   PWM_G_STOP; R_OFF; G_OFF; B_OFF
#define WHITE PWM_G_STOP; R_ON;  G_ON;  B_ON

#define RANDOM_SCALED rand() / (RAND_MAX / 0xFF) /

#define MODES_COUNT 2
unsigned char mode = 0;

ISR(PCINT0_vect) {
	if (BTN_PRESSED) {  // Falling edge only
		BTN_INT_DISABLE;  // Antidrbezg
		WATCHDOG_TIMER_RESET;
		mode = (mode + 1) % MODES_COUNT;
	}
}

ISR(WDT_vect) {  // Watchdog Timer Interrupt, fires every 1 second (WDTCR).
	BTN_INT_ENABLE;  // Antidrbezg ends
}

#define RANDOM_WAIT_T 30  // Scale (maximum delay time)
void random_wait(int low, int scale){
	int r = low + rand() / scale;
	while (r--) _delay_us(RANDOM_WAIT_T);
}

void main_loop() {
	switch (mode) {
		case 0:
			OFF;
			break;
		case 1:
			PWM_G_START;
			//B_ON; OCR0A = 50 + RANDOM_SCALED 4;
			//R_ON; OCR0A = RANDOM_SCALED 7;
			

			R_ON; OCR0A = 40;
			random_wait(0, 5);
			R_OFF; PWM_G_DISCONNECT;
			random_wait(0, 2);


			break;
	}
}

int main() {
	DDRB  &= ~(1 << BTN_PIN);  // Button pin = Input
	PORTB |=  (1 << BTN_PIN);  // Button pull-up to VCC
	DDRB  |=  (1 << DDB0) | (1 << DDB1) | (1 << DDB2);  // LED pins = Outputs
	
	TCCR0A = (1 << WGM00) | (1 << WGM01); // Fast PWM
	GIMSK  =  1 << PCIE;  // Pin Change Interrupts Enable
	BTN_INT_ENABLE;

	WDTCR  =  1 << WDTIE; // Watchdog Timer -> Interrupt Mode
	WDTCR |= (1 << WDP2) | (1 << WDP1);  // Watchdog Timer Prescale = 1s

	sei();  // Global interrupts enable
    
	OFF;

	while (1)
		main_loop();
}

